package com.example.lynn.guessarea;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.example.lynn.guessarea.MainActivity.*;

/**
 * Created by lynn on 6/16/2015.
 */
public class MyListener implements View.OnClickListener,View.OnTouchListener {
    private float offsetX;
    private float offsetY;
    private RelativeLayout.LayoutParams layoutParams;

    @Override
    public void onClick(View v) {
        final Map<String,Double> areas = ButtonView.getAreas();

        if (v == playGame) {
            List<String> list = new ArrayList<>();

            list.addAll(areas.keySet());

            int[] indices = new int[4];

            indices[0] = (int)(list.size()*Math.random());

            Set<Integer> usedIndices = new HashSet<>();

            usedIndices.add(indices[0]);

            for (int counter=1;counter<indices.length;counter++) {
                int temp = (int)(list.size()*Math.random());

                while (usedIndices.contains(temp))
                    temp = (int)(list.size()*Math.random());

                indices[counter] = temp;

                usedIndices.add(temp);
            }

            for (int counter=0;counter<states.length;counter++) {
                states[counter].setText(list.get(indices[counter]));

                answers[counter].setText("");
            }

            message.setText("");
        } else if (v == submitAnswer) {
            Toast.makeText(v.getContext(),"Hello",Toast.LENGTH_LONG).show();

            TreeSet<Button> temp = new TreeSet<>(new Comparator<Button>() {

                @Override
                public int compare(Button button1, Button button2) {
                    RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams)button1.getLayoutParams();
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)button2.getLayoutParams();

                    if (layoutParams1.leftMargin < layoutParams2.leftMargin)
                        return(-1);
                    else if (layoutParams1.leftMargin > layoutParams2.leftMargin)
                        return(1);
                    else
                        return 0;
                }

            });

            for (int counter=0;counter<states.length;counter++)
                temp.add(states[counter]);

            Button[] buttonsOrderedByPosition = temp.toArray(new Button[temp.size()]);

            temp = new TreeSet<>(new Comparator<Button>() {

                @Override
                public int compare(Button button1, Button button2) {
                    if (areas.get(button1.getText()) > areas.get(button2.getText()))
                        return(-1);
                    else if (areas.get(button1.getText()) < areas.get(button2.getText()))
                        return(1);
                    else
                        return 0;
                }

            });

            for (int counter=0;counter<states.length;counter++)
                temp.add(states[counter]);

            Button[] buttonsInCorrectOrder = temp.toArray(new Button[temp.size()]);

            if (Arrays.equals(buttonsOrderedByPosition, buttonsInCorrectOrder))
                message.setText("Correct");
            else {
                message.setText("Incorrect");

                for (int counter=0;counter<buttonsInCorrectOrder.length;counter++) {
                    String state = buttonsInCorrectOrder[counter].getText().toString();

                    double area = areas.get(state);

                    answers[counter].setText(state.substring(0, 1).toUpperCase() + state.substring(1).toLowerCase() + " has " + area + " square miles");
                }
            }
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offsetX = event.getRawX() - layoutParams.leftMargin;
            offsetY = event.getRawY() - layoutParams.topMargin;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            layoutParams.leftMargin = (int)(event.getRawX() - offsetX);
            layoutParams.topMargin = (int)(event.getRawY() - offsetY);

            v.setLayoutParams(layoutParams);
        }

        return(true);
    }
}
