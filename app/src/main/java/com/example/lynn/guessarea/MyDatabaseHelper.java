package com.example.lynn.guessarea;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lynn on 6/16/2015.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "StateAreas";

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE = "CREATE TABLE statearea(state TEXT NOT NULL,area REAL NOT NULL);";

    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(DATABASE_CREATE);

       db.execSQL("INSERT INTO statearea VALUES('Alaska','665384.04');");
       db.execSQL("INSERT INTO statearea VALUES('Texas','268596.46');");
       db.execSQL("INSERT INTO statearea VALUES('California','163694.74');");
       db.execSQL("INSERT INTO statearea VALUES('Montana','147039.71');");
       db.execSQL("INSERT INTO statearea VALUES('New Mexico','121590.30');");
       db.execSQL("INSERT INTO statearea VALUES('Arizona','113990.30');");
       db.execSQL("INSERT INTO statearea VALUES('Nevada','110571.82');");
       db.execSQL("INSERT INTO statearea VALUES('Colorado','104093.67');");
       db.execSQL("INSERT INTO statearea VALUES('Oregon','98378.54');");
       db.execSQL("INSERT INTO statearea VALUES('Wyoming','97813.01');");
       db.execSQL("INSERT INTO statearea VALUES('Michigan','96713.51');");
       db.execSQL("INSERT INTO statearea VALUES('Minnesota','86935.83');");
       db.execSQL("INSERT INTO statearea VALUES('Utah','84896.88');");
       db.execSQL("INSERT INTO statearea VALUES('Idaho','83568.95');");
       db.execSQL("INSERT INTO statearea VALUES('Kansas','82278.36');");
       db.execSQL("INSERT INTO statearea VALUES('Nebraska','77347.81');");
       db.execSQL("INSERT INTO statearea VALUES('South Dakota','77115.68');");
       db.execSQL("INSERT INTO statearea VALUES('Washington','71297.95');");
       db.execSQL("INSERT INTO statearea VALUES('North Dakota','70698.32');");
       db.execSQL("INSERT INTO statearea VALUES('Oklahoma','69898.87');");
       db.execSQL("INSERT INTO statearea VALUES('Missouri','69706.99');");
       db.execSQL("INSERT INTO statearea VALUES('Florida','65757.70');");
       db.execSQL("INSERT INTO statearea VALUES('Wisconsin','65496.38');");
       db.execSQL("INSERT INTO statearea VALUES('Georgia','59425.15');");
       db.execSQL("INSERT INTO statearea VALUES('Illinois','57913.55');");
       db.execSQL("INSERT INTO statearea VALUES('Iowa','56272.81');");
       db.execSQL("INSERT INTO statearea VALUES('New York','54554.98');");
       db.execSQL("INSERT INTO statearea VALUES('North Carolina','53819.16');");
       db.execSQL("INSERT INTO statearea VALUES('Arkansas','53178.55');");
       db.execSQL("INSERT INTO statearea VALUES('Alabama','52420.07');");
       db.execSQL("INSERT INTO statearea VALUES('Louisiana','52378.13');");
       db.execSQL("INSERT INTO statearea VALUES('Mississippi','48431.78');");
       db.execSQL("INSERT INTO statearea VALUES('Pennsylvania','46054.35');");
       db.execSQL("INSERT INTO statearea VALUES('Ohio','44825.58');");
       db.execSQL("INSERT INTO statearea VALUES('Virginia','42774.93');");
       db.execSQL("INSERT INTO statearea VALUES('Tennessee','42144.25');");
       db.execSQL("INSERT INTO statearea VALUES('Kentucky','40407.80');");
       db.execSQL("INSERT INTO statearea VALUES('Indiana','36419.55');");
       db.execSQL("INSERT INTO statearea VALUES('Maine','35379.74');");
       db.execSQL("INSERT INTO statearea VALUES('South Carolina','32020.49');");
       db.execSQL("INSERT INTO statearea VALUES('West Virginia','24230.04');");
       db.execSQL("INSERT INTO statearea VALUES('Maryland','12405.93');");
       db.execSQL("INSERT INTO statearea VALUES('Hawaii','10931.72');");
       db.execSQL("INSERT INTO statearea VALUES('Massachusetts','10554.39');");
       db.execSQL("INSERT INTO statearea VALUES('Vermont','9616.36');");
       db.execSQL("INSERT INTO statearea VALUES('New Hampshire','9349.16');");
       db.execSQL("INSERT INTO statearea VALUES('New Jersey','8722.58');");
       db.execSQL("INSERT INTO statearea VALUES('Connecticut','5543.41');");
       db.execSQL("INSERT INTO statearea VALUES('Delaware','2488.72');");
       db.execSQL("INSERT INTO statearea VALUES('Rhode Island','1544.89');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
