package com.example.lynn.guessarea;

import android.content.Context;
import android.widget.RelativeLayout;

import static com.example.lynn.guessarea.MainActivity.*;

/**
 * Created by lynn on 6/16/2015.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        playGame.setText("Play Game");

        playGame.setLayoutParams(layoutParams);

        playGame.setOnClickListener(listener);

        addView(playGame);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,400);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.playgame);

        buttonView.setLayoutParams(layoutParams);

        addView(buttonView);

        for (int counter=0;counter<states.length;counter++)
            states[counter].setOnTouchListener(listener);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.buttonview);

        submitAnswer.setText("Submit Answer");

        submitAnswer.setLayoutParams(layoutParams);

        submitAnswer.setOnClickListener(listener);

        addView(submitAnswer);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.submitanswer);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.buttonview);

        message.setLayoutParams(layoutParams);

        addView(message);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.submitanswer);

        answers[0].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer1);

        answers[1].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer2);

        answers[2].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer3);

        answers[3].setLayoutParams(layoutParams);

        for (int counter=0;counter<answers.length;counter++)
            addView(answers[counter]);
    }

}


