package com.example.lynn.guessarea;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    public static MyDatabaseHelper myDatabaseHelper;
    public static Button playGame;
    public static Button submitAnswer;
    public static TextView message;
    public static ButtonView buttonView;
    public static MyListener listener;
    public static Button[] states;
    public static SQLiteDatabase database;
    public static TextView[] answers;
    public static int[] iDsForAnswers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myDatabaseHelper = new MyDatabaseHelper(this);

        database = myDatabaseHelper.getReadableDatabase();

        playGame = new Button(this);

        playGame.setId(R.id.playgame);

        submitAnswer = new Button(this);

        submitAnswer.setId(R.id.submitanswer);

        states = new Button[4];

        for (int counter=0;counter<states.length;counter++)
            states[counter] = new Button(this);

        buttonView = new ButtonView(this);

        buttonView.setId(R.id.buttonview);

        message = new TextView(this);

        message.setId(R.id.message);

        listener = new MyListener();

        answers = new TextView[4];

        iDsForAnswers = new int[]{R.id.answer1,R.id.answer2,R.id.answer3,R.id.answer4};

        for (int counter=0;counter<answers.length;counter++) {
            answers[counter] = new TextView(this);

            answers[counter].setId(iDsForAnswers[counter]);
        }

        setContentView(new MyView(this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
