package com.example.lynn.guessarea;

import android.content.Context;
import android.database.Cursor;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;

import static com.example.lynn.guessarea.MainActivity.*;

/**
 * Created by lynn on 6/16/2015.
 */
public class ButtonView extends RelativeLayout {

    public static Map<String,Double> getAreas() {
       Map<String,Double> areas = new HashMap<>();

       Cursor cursor = database.rawQuery("SELECT * FROM statearea;",new String[]{});

       cursor.moveToFirst();

       do {
           int stateIndex = cursor.getColumnIndex("state");
           int areaIndex = cursor.getColumnIndex("area");

           String state = cursor.getString(stateIndex);
           double area = cursor.getDouble(areaIndex);

           areas.put(state.toUpperCase(), area);
       } while (cursor.moveToNext());

       return(areas);
    }

    public ButtonView(Context context) {
        super(context);

        for (int counter=0;counter<states.length;counter++)
            addView(states[counter]);
    }

}
